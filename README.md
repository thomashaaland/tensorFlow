# TensorFlow CNN Recognise Handwriting

This project attempts to recognise handwriting. The project is in jupyter notebook and consists of preprocessing of handwritten letters and use TensorFlow in an attempt at recognising the letters.

After some preprocessing, bootstrapping and optimizing of the CNN a final accuracy of 84.4% was obtained. This is a considerable improvement over the earlier 51%. The neural network alone, granted enough epochs is a massive improvement skirting 80% accuracy, though after bootstrapping and feature engineering a more robust improvement is obtainable.

![graph](finalGraph.png)

## Feature engineering
Since the original data has a systematic error where the leftmost letters are systematically different to the rightmost letters, I switched to including every other letter in both the training and testing data. Without this effort I got considerable lower acuracy. This is sort of cheating since I have seen the whole dataset beforehand and can take decisions based on that. However, it also shows both that it can be important to randomize the data (depending on the nature of the data) and that once deployed the model need constant updating since new data can be biased in some new way not seen by the model. 

## Install
To install the script requires that you have jupyter notebook installed. Install (Anaconda)[www.anaconda.com] and select jupyter notebook.

## Usage
To run the script open the file handWritingRecogniseTensorFlow.ipynb. The script handles dependencies when you run it.

## Contributing
Send questions and suggestions to <thomas.haaland@gmail.com>

UNLICENSED
